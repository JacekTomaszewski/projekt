package com.aravae.agile.controller;

import com.aravae.agile.model.dto.ProjectDTO;
import com.aravae.agile.model.entity.Project;
import com.aravae.agile.service.ProjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author SSzeszko
 */
@RestController
@RequestMapping("/project")
public class ProjectController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectController.class);

    @Autowired
    private ProjectService projectService;

    @RequestMapping(path = "/get", method = RequestMethod.GET)
    public @ResponseBody
    List<ProjectDTO> getProjects() {
        return projectService.getListProject();
    }

//    @RequestMapping(path = "/add", method = RequestMethod.POST)
//    public @ResponseBody
//    Project add(@RequestBody Project project) {
//        projectService.saveOrUpdate(project);
//        return project;
//    }
//
//
//    @RequestMapping(path = "/delete/{id}", method = RequestMethod.DELETE)
//    public void delete(@PathVariable("id") Integer id) {
//        projectService.deleteProject(id);
//    }
//
    @RequestMapping(path = "/deleteMultiple", method = RequestMethod.POST)
    public void deleteMultiple(@RequestBody List<Integer> ids){
        projectService.deleteMultiple(ids);
    }
}
