package com.aravae.agile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 *
 * @author SSzeszko
 */
@SpringBootApplication
public class AgileLabRestApp extends SpringBootServletInitializer {
    
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(AgileLabRestApp.class);
    }
    
    public static void main(String[] args) {
        SpringApplication.run(AgileLabRestApp.class, args);
    }
}
