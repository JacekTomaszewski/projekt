package com.aravae.agile.service;

import com.aravae.agile.com.aravae.agile.dao.ProjectDao;
import com.aravae.agile.model.dto.ProjectDTO;
import com.aravae.agile.model.entity.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    ProjectDao projectDao;

    @Override
    public List<ProjectDTO> getListProject() {
        return projectDao.getListProject();
    }

    @Override
    public void deleteMultiple(List<Integer> ids) {
        for(Integer id : ids){
            projectDao.deleteProject(id);
        }
    }

//    @Override
//    public void saveOrUpdate(Project project) {
//        projectDao.saveOrUpdate(project);
//    }
//
//    @Override
//    public void deleteProject(Integer id) {
//        projectDao.deleteProject(id);
//    }
//
//    @Override
//    public Project findProjectById(Integer id) {
//        return projectDao.findProjectById(id);
//    }
}
