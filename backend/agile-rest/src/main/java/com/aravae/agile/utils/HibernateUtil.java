/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aravae.agile.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author SSzeszko
 */
public class HibernateUtil {

    private static final String DATA_SOURCE = "psqlManager";
    private EntityManagerFactory entityManagerFactory;
    private static HibernateUtil instance;

    private HibernateUtil() {
    }

    public synchronized static HibernateUtil getManager() {
        if (instance == null) {
            instance = new HibernateUtil();
        }
        return instance;
    }

    public EntityManagerFactory createEntityManagerFactory() {
        if (entityManagerFactory == null) {
            entityManagerFactory = Persistence.createEntityManagerFactory(DATA_SOURCE);
        }
        return entityManagerFactory;
    }

    public EntityManager createEntityManager() {
        return this.createEntityManagerFactory().createEntityManager();
    }

    public void closeEntityManagerFactory() {
        if (entityManagerFactory != null) {
            entityManagerFactory.close();
        }
    }

    private static class Inner {
        private static final HibernateUtil INSTANCE = new HibernateUtil();
    }

    public static HibernateUtil getInstance() {
        return Inner.INSTANCE;
    }

}
