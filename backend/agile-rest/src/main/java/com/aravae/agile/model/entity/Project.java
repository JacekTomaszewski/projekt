package com.aravae.agile.model.entity;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author SSzeszko
 */
@Entity
@Table(name = "projekt")
@NamedQueries({
        @NamedQuery(name = "Project.findAll", query = "SELECT p FROM Project p")
        , @NamedQuery(name = "Project.findByProjektId", query = "SELECT p FROM Project p WHERE p.id IN :projektIds")
        , @NamedQuery(name = "Project.findByNazwa", query = "SELECT p FROM Project p WHERE p.nazwa = :nazwa")
        , @NamedQuery(name = "Project.findByOpis", query = "SELECT p FROM Project p WHERE p.opis = :opis")
        , @NamedQuery(name = "Project.findByDataczasUtworzenia", query = "SELECT p FROM Project p WHERE p.dataczasUtworzenia = :dataczasUtworzenia")
        , @NamedQuery(name = "Project.findByDataOddania", query = "SELECT p FROM Project p WHERE p.dataOddania = :dataOddania")
        , @NamedQuery(name = "Project.findByIdGreater", query = "SELECT p FROM Project p WHERE p.id > :projektId")})
public class Project extends BaseEntity {

    @Column(name = "nazwa", nullable = false)
    @Basic(optional = false)
    private String nazwa;
    @Column(name = "opis")
    private String opis;
    @Column(name = "dataczas_utworzenia")
    private LocalDateTime dataczasUtworzenia;
    @Column(name = "data_oddania")
    private LocalDate dataOddania;
    @OneToMany(mappedBy = "projekt", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Zadanie> zadania;
    @ManyToMany
    @JoinTable(name = "projekt_student",
            joinColumns = {
                    @JoinColumn(name = "projekt_id")},
            inverseJoinColumns = {
                    @JoinColumn(name = "student_id")})
    private List<Student> studenci;

    public List<Zadanie> getZadania() {
        return zadania;
    }

    public void setZadania(List<Zadanie> zadania) {
        this.zadania = zadania;
    }

    public Project() {
    }

    public Project(Integer projektId) {
        this.id = projektId;
    }

    public Project(Integer projektId, String nazwa) {
        this.id = projektId;
        this.nazwa = nazwa;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public LocalDateTime getDataczasUtworzenia() {
        return dataczasUtworzenia;
    }

    public void setDataczasUtworzenia(LocalDateTime dataczasUtworzenia) {
        this.dataczasUtworzenia = dataczasUtworzenia;
    }

    public LocalDate getDataOddania() {
        return dataOddania;
    }

    public void setDataOddania(LocalDate dataOddania) {
        this.dataOddania = dataOddania;
    }

    public List<Student> getStudenci() {
        return studenci;
    }

    public void setStudenci(List<Student> studenci) {
        this.studenci = studenci;
    }

}
