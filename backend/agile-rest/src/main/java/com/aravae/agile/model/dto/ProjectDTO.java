package com.aravae.agile.model.dto;

import com.aravae.agile.model.entity.Zadanie;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.util.List;

/**
 *
 * @author SSzeszko
 */
public class ProjectDTO extends IdDTO {

    private String nazwa;
    private String opis;
    @JsonManagedReference
    public List<ZadanieDTO> zadania;

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public List<ZadanieDTO> getZadania() {
        return zadania;
    }

    public void setZadania(List<ZadanieDTO> zadania) {
        this.zadania = zadania;
    }
}
