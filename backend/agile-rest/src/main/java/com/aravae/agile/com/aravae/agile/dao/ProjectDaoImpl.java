package com.aravae.agile.com.aravae.agile.dao;

import com.aravae.agile.model.dto.ProjectDTO;
import com.aravae.agile.model.entity.Project;
import com.aravae.agile.utils.ObjectMapperUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class ProjectDaoImpl implements ProjectDao {

    @PersistenceContext()
    protected EntityManager em;

//    protected Session getSession() {
//        return em.getCurrentSession();
//    }

    @Override
    public List<ProjectDTO> getListProject() {
        List<ProjectDTO> result;
        result = ObjectMapperUtils.mapAll(em.createNamedQuery("Project.findAll").getResultList(), ProjectDTO.class);
        return result;
    }

//    @Override
//    public void save(ProjectDTO project) {
//        getSession().saveOrUpdate(project);
//    }
//
    @Override
    public void deleteProject(Integer id) {
        Project project = em.find(Project.class, id);
        em.remove(project);
    }
//
//    @Override
//    public Project findProjectById(Integer id) {
//        return getSession().get(Project.class, id);
//    }
}
