package com.aravae.agile.com.aravae.agile.dao;

import com.aravae.agile.model.dto.ProjectDTO;
import com.aravae.agile.model.entity.Project;

import java.util.List;

public interface ProjectDao {

    public List<ProjectDTO> getListProject();

//    public void saveOrUpdate(Project project);
//
    public void deleteProject(Integer id);
//
//    public Project findProjectById(Integer id);
}
