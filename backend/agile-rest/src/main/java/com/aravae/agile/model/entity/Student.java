package com.aravae.agile.model.entity;

import java.util.List;
import javax.persistence.*;

/**
 *
 * @author SSzeszko
 */
@Entity
@Table(name = "student")
@NamedQueries({
    @NamedQuery(name = "Student.findAll", query = "SELECT s FROM Student s")
    , @NamedQuery(name = "Student.findByStudentId", query = "SELECT s FROM Student s WHERE s.id = :studentId")
    , @NamedQuery(name = "Student.findByImie", query = "SELECT s FROM Student s WHERE s.imie = :imie")
    , @NamedQuery(name = "Student.findByNazwisko", query = "SELECT s FROM Student s WHERE s.nazwisko = :nazwisko")
    , @NamedQuery(name = "Student.findByNrIndeksu", query = "SELECT s FROM Student s WHERE s.nrIndeksu = :nrIndeksu")
    , @NamedQuery(name = "Student.findByEmail", query = "SELECT s FROM Student s WHERE s.email = :email")
    , @NamedQuery(name = "Student.findByStacjonarny", query = "SELECT s FROM Student s WHERE s.stacjonarny = :stacjonarny")})
public class Student extends BaseEntity{
    @Column(name = "imie", nullable = false)
    private String imie;
    @Column(name = "nazwisko", nullable = false)
    private String nazwisko;
    @Column(name = "nr_indeksu")
    private String nrIndeksu;
    @Column(name = "email")
    private String email;
    @Column(name = "stacjonarny")
    private Boolean stacjonarny;
    @ManyToMany(mappedBy = "studenci")
    private List<Project> projekty; 
    
    public Student() {
    }

    public Student(Integer studentId) {
        this.id = studentId;
    }

    public Student(String imie, String nazwisko, String nrIndeksu) {   
        this.imie = imie;   
        this.nazwisko = nazwisko;   
        this.nrIndeksu = nrIndeksu;   
    }

    public Student(String imie, String nazwisko, String nrIndeksu, String email,      Boolean stacjonarny) {   
        this.imie = imie;   
        this.nazwisko = nazwisko;   
        this.nrIndeksu = nrIndeksu;  
        this.email = email;   
        this.stacjonarny = stacjonarny;  
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getNrIndeksu() {
        return nrIndeksu;
    }

    public void setNrIndeksu(String nrIndeksu) {
        this.nrIndeksu = nrIndeksu;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getStacjonarny() {
        return stacjonarny;
    }

    public void setStacjonarny(Boolean stacjonarny) {
        this.stacjonarny = stacjonarny;
    }

    public List<Project> getProjekty() {
        return projekty;
    }

    public void setProjekty(List<Project> projekty) {
        this.projekty = projekty;
    }
    
}
