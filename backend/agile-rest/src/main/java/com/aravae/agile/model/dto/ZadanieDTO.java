package com.aravae.agile.model.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;

public class ZadanieDTO extends IdDTO{
    private String nazwa;
    private String opis;
    @JsonBackReference
    private ProjectDTO projekt;

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public ProjectDTO getProjekt() {
        return projekt;
    }

    public void setProjekt(ProjectDTO projekt) {
        this.projekt = projekt;
    }
}
