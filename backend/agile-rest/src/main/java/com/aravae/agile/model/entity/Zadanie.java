package com.aravae.agile.model.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 *
 * @author SSzeszko
 */
@Entity
@Table(name = "zadanie")
@NamedQueries({
    @NamedQuery(name = "Zadanie.findAll", query = "SELECT z FROM Zadanie z")
    , @NamedQuery(name = "Zadanie.findByZadanieId", query = "SELECT z FROM Zadanie z WHERE z.id = :zadanieId")
    , @NamedQuery(name = "Zadanie.findByNazwa", query = "SELECT z FROM Zadanie z WHERE z.nazwa = :nazwa")
    , @NamedQuery(name = "Zadanie.findByKolejnosc", query = "SELECT z FROM Zadanie z WHERE z.kolejnosc = :kolejnosc")
    , @NamedQuery(name = "Zadanie.findByOpis", query = "SELECT z FROM Zadanie z WHERE z.opis = :opis")
    , @NamedQuery(name = "Zadanie.findByDataczasOddania", query = "SELECT z FROM Zadanie z WHERE z.dataczasOddania = :dataczasOddania")})
public class Zadanie extends BaseEntity {

    @Column(name = "nazwa", nullable = false)
    private String nazwa;
    @Column(name = "kolejnosc")
    private int kolejnosc;
    @Column(name = "opis")
    private String opis;
    @Column(name = "dataczas_oddania")
    private LocalDateTime dataczasOddania;
    @ManyToOne
    @JoinColumn(name = "projekt_id")
    private Project projekt;

    public Zadanie() {
    }

    public Zadanie(Integer zadanieId) {
        this.id = zadanieId;
    }

    public Zadanie(Integer zadanieId, String nazwa, int kolejnosc) {
        this.id = zadanieId;
        this.nazwa = nazwa;
        this.kolejnosc = kolejnosc;
    }

    public Project getProjekt() {
        return projekt;
    }

    public void setProjekt(Project projekt) {
        this.projekt = projekt;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public int getKolejnosc() {
        return kolejnosc;
    }

    public void setKolejnosc(int kolejnosc) {
        this.kolejnosc = kolejnosc;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public LocalDateTime getDataczasOddania() {
        return dataczasOddania;
    }

    public void setDataczasOddania(LocalDateTime dataczasOddania) {
        this.dataczasOddania = dataczasOddania;
    }

}
