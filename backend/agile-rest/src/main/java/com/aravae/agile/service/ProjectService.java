package com.aravae.agile.service;

import com.aravae.agile.model.dto.ProjectDTO;
import com.aravae.agile.model.entity.Project;

import java.util.List;

public interface ProjectService {

    List<ProjectDTO> getListProject();

    void deleteMultiple(List<Integer> ids);

//    void saveOrUpdate(Project project);
//
//    void deleteProject(Integer id);
//
//    Project findProjectById(Integer id);
}
