import React, { Component } from "react";
import "../styles/Home.css";

export default class Home extends Component {
  render() {
    return (
      <div className="Home">
        <div className="lander">
          <h1>Replanneract</h1>
          <p>Timesheet for programmers</p>
        </div>
      </div>
    );
  }
}