import React from "react";
import "../styles/NotFound.css";

export default () =>
  <div className="NotFound">
    <h3>Page not found!</h3>
  </div>;