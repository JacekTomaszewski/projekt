import React, { Component } from "react";
import { Button } from "react-bootstrap";
import "../styles/SignUp.css";
import { FormErrors } from './FormErrors';

export default class SignUp extends Component {
    constructor (props) {
        super(props);
        this.state = {
          email: '',
          password: '',
          formErrors: {email: '', password: ''},
          emailValid: false,
          passwordValid: false,
          formValid: false
        }
      }
    
      handleUserInput = (e) => {
        const id = e.target.id;
        const value = e.target.value;
        this.setState({[id]: value},
                      () => { this.validateField(id, value) });
      }
    
      validateField(fieldId, value) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;
    
        switch(fieldId) {
          case 'email':
            emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
            fieldValidationErrors.email = emailValid ? '' : ' is invalid';
            break;
          case 'password':
            passwordValid = value.length >= 6;
            fieldValidationErrors.password = passwordValid ? '': ' is too short';
            break;
          default:
            break;
        }
        this.setState({formErrors: fieldValidationErrors,
                        emailValid: emailValid,
                        passwordValid: passwordValid
                      }, this.validateForm);
      }
    
      validateForm() {
        this.setState({formValid: this.state.emailValid && this.state.passwordValid});
      }


      
    errorClass(error) {
    return(error.length === 0 ? '' : 'has-error');
    }
    
  render() {
    return (
      <div className="SignUp">
        <form onSubmit={this.handleSubmit}>
        <div className="panel panel-default" style={{textAlignVertical: "center",textAlign: "center",}}>
          <FormErrors formErrors={this.state.formErrors} />
        </div>
        <div class="row">
            <div class="col-md-12 mx-auto">
                <div className={`form-group ${this.errorClass(this.state.formErrors.email)}`}>
                    <input type="text" id="email" class="form-control" value={this.state.email} onChange={this.handleUserInput} required></input>
                    <label class="form-control-placeholder" for="email">Email</label>
                </div>
                <div className={`form-group ${this.errorClass(this.state.formErrors.password)}`}>
                    <input type="password" id="password" class="form-control" value={this.state.password} onChange={this.handleUserInput} required></input>
                    <label class="form-control-placeholder" for="password">Password</label>
                </div>
            </div>
        </div>
          <Button
            block
            bsSize="large"
            disabled={!this.state.formValid}
            type="submit"
          >
            SignUp
          </Button>
        </form>
      </div>
    )
  }
}