import React, { Component } from "react";
import "../styles/Projects.css";
import axios from "axios";
import { Button } from "react-bootstrap";
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';

export default class Projects extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedProjects : [],
            projectList: []
        };
        axios.get("http://localhost:8081/agile/project/get", {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
            }
        })
            .then(response => {
                this.setState({projectList: response.data});
            })
    }

    handleOnSelect = (row, isSelect) => {
        if (isSelect) {
          this.setState(() => ({
            selectedProjects: [...this.state.selectedProjects, row.id]
          }));
        } else {
          this.setState(() => ({
            selectedProjects: this.state.selectedProjects.filter(x => x !== row.id)
          }));
        }
      }

      handleBtnClick = () => {
        console.log(this.state.selectedProjects);
        axios.post("http://localhost:8081/agile/project/delete", {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
            },
            ids: this.state.selectedProjects
        })
            .then(response => {
            })
      }
    
      handleOnSelectAll = (isSelect, rows) => {
        const ids = rows.map(r => r.id);
        if (isSelect) {
          this.setState(() => ({
            selectedProjects: ids
          }));
        } else {
          this.setState(() => ({
            selectedProjects: []
          }));
        }
      }


    render() {
        const columns = [{
            dataField: 'id',
            text: 'Project ID'
        }, {
            dataField: 'nazwa',
            text: 'Project Name'
        }, {
            dataField: 'opis',
            text: 'Project Description'
        }];
        const projectStyle = {
            display: "flex",
            justifyContent: "center",
        }
        const selectRow = {
            mode: 'checkbox',
            bgColor: 'pink',
            clickToSelect: true,
            selected: this.state.selectedProjects,
            onSelect: this.handleOnSelect,
            onSelectAll: this.handleOnSelectAll
        };

        const paginationOptions = {
            pageStartIndex: 0,
            paginationSize: 3,
            showTotal: true,
            sizePerPageList: [{
                text: '5', value: 5
            }, {
                text: '10', value: 10
            }, {
                text: 'All', value: this.state.projectList.length
            }],
            hidePageListOnlyOnePage: true,
            onPageChange: (page, sizePerPage) => { },
            onSizePerPageChange: (sizePerPage, page) => { },
        };
        return (
            <div>
                <BootstrapTable
                    style
                    selectRow={selectRow}
                    keyField='id'
                    data={this.state.projectList}
                    columns={columns}
                    pagination={paginationFactory(paginationOptions)}
                />
                <Button bsStyle="danger" onClick={ this.handleBtnClick }>Delete selected Projects</Button>
            </div>
        )
    }
}