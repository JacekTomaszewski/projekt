import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./containers/Home";
import NotFound from "./containers/NotFound";
import SignUp from "./containers/SignUp";
import Projects from "./containers/Projects";

export default () =>
  <Switch>
    <Route path="/signup" exact component={SignUp} />
    <Route path="/projects" exact component={Projects} />
    <Route path="/" exact component={Home} />
    <Route component={NotFound} />
  </Switch>;